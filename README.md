# CS3060_FA2018_R06_ZIMCOSKY

## Reflection Questions

1. What was the major question or topic addressed in the classroom?
   - How to solve puzzles by describing a solved puzzle with rules in prolog.

2. Summarize what you learned during this week of class. Note any particular features, concepts, code,or terms that you believe are important.
   - I learned about the many differences between programming in prolog using gprolog, and SWI-Prolog. For example, in my sudoku solver below, we made a rule stating that a list is valid if every element of a list is different. I use gprolog, but still included commented out SWI-Prolog statements to show the differences between the two different prolog implimentations.

   ```prolog
   % SWI-Prolog only
   % :- use_module(library(clpfd)).
   ```

   The above library was not used in gprolog, as you will see.

   ```prolog
   valid([]).
   valid([Head|Tail]) :-
       %SWI-Prolog only
       %all_distinct(Head),
       %gProlog only
       fd_all_different(Head),
       valid(Tail).
    ```

   In the code above, we can see that gprolog and SWI-Prolog have different standard rules that achieve the same goal in this program.

    ```prolog
    % SWI-Prolog only. Not needed for gProlog
    % valid_domain(List, Min, Max) :- List ins Min..Max.

    sudoku(Puzzle, Solution) :-
           Solution = Puzzle,
           Puzzle = [S11, S12, S13, S14, S15, S16, S17, S18, S19,
                     S21, S22, S23, S24, S25, S26, S27, S28, S29,
                     S31, S32, S33, S34, S35, S36, S37, S38, S39,
                     S41, S42, S43, S44, S45, S46, S47, S48, S49,
                     S51, S52, S53, S54, S55, S56, S57, S58, S59,
                     S61, S62, S63, S64, S65, S66, S67, S68, S69,
                     S71, S72, S73, S74, S75, S76, S77, S78, S79,
                     S81, S82, S83, S84, S85, S86, S87, S88, S89,
                     S91, S92, S93, S94, S95, S96, S97, S98, S99
                     ],

           % fd_domain exists in gProlog, not SWI-Prolog
           fd_domain(Solution, 1, 4),
           % valid_domain(Solution, 1, 9),
           ```

           Above you can see where the SWI-Prolog implimentation used the library from the top block of code. The library used provided the ability to write a defining rule for valid_domain() using a dot formatting. Using gprolog, I didn't have to call upon the library, or even create such a rule, because a similar standard gprolog rule already exists.

3. What did you learn about the current language that is different from what you have previously experienced? Do you think it is a good difference or bad difference?
   - Writing prolog is backwards in a sense. Instead of articulating what you need to do to achieve a goal, you express the goal itself in enough detail that the compiler can find ways to achieve the goal with any given information.

4. Do you learn answers to any of the 5 fundamental questions we are asking about each programming language this week?

   1. What is the typing model?
      - Strongly statically typed

   2. What is the programming model?
      - Logical declaritive language

   3. Is the language Interpreted or Compiled?
      - compiled

   4. What are the decision constructs & core data structures?
      - Facts, rules, querys and knowlege bases

   5. What makes this language unique?
      - Instead of preforming step-by-step commands to form a solution, prolog revolves around defining and solving logical formulas.

5. Did anyone modify the sudoku solver in a different manner? Do you think this code could also generate sudoku puzzles?
   - I didn't notice anyone doing anything different from me (except for people using SWI-Prolog).
   - I think it would be able to if rather than numbers in the Puzzle list along with the dont care underscores, you called unique variables.


6. What  was  the  most  difficult  aspect  of  what  you  learned  in  class?  Why  was  this  the  most  difficult aspect? What can you do to make it easier for you?
   - The most difficult aspect for me was trying to find ways of formatting my output. I could tell the output was right in a way, but instead of being displayed as multiple lists showing different possible solutions, it seemed like it was displaying on list with different vaiable ranges for each item. I later discovered that I needed to call *fd_labeling()* with the variable holding my solutions as a parameter along with the statement I use to solve my puzzle.